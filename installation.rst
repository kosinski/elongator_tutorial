Install Assembline
==================

Install Assembline following `instructions in the Manual <https://assembline.readthedocs.io/en/latest/installation.html#installation-with-anaconda>`_.