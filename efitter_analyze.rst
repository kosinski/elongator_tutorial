Analyze fits
============

Check if run correctly
----------------------

If everything run correctly, the fits directory should contain the following structure: ::

    fits/
        search100000_metric_cam_inside0.6/
            emd_4151_binned.mrc/
                Elp1.CTD.on5cqs.5cqr.model_ElNemo_mode7.pdb/
                    Rplots.pdf
                    emd_4151_binned.mrc
                    histogram.png
                    log_err.txt
                    log_out.txt
                    ori_pdb.pdb
                    run.sh
                    solutions.csv
                    solutions_pvalues.csv
                Elp1_NTD_1st_propeller.pdb/
                    Rplots.pdf
                    emd_4151_binned.mrc
                    histogram.png
                    log_err.txt
                    log_out.txt
                    ori_pdb.pdb
                    run.sh
                    solutions.csv
                    solutions_pvalues.csv

                ... and so on

.. warning::
    The **solutions_pvalues.csv** files contain the fit libraries and must be present for the next step to run.

