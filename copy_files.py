import os
import shutil

items = [
    'EM_data',
    'in_pdbs',
    'xlinks',
    'fits',
    'elp_sequences.fasta',
    'efitter_params.py',

    'elongator.json',
    'params.py',
    'params_refine.py',
    'elongator_refine_template.json'

]

DIR='../../old_pipeline/SuperConfig/test/data/Elongator/'

if os.path.exists('Elongator'):
    shutil.rmtree('Elongator')
os.makedirs('Elongator', exist_ok=True)


for item in items:
    try:
        shutil.copy(os.path.join(DIR, item), 'Elongator')
    except IsADirectoryError:
        shutil.copytree(os.path.join(DIR, item), os.path.join('Elongator', item))
