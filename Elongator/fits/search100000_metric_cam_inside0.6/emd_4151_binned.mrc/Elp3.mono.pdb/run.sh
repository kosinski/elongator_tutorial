#!/bin/bash
#
#SBATCH --job-name=search100000_metric_cam_inside0.6_emd_4151_binned.mrc_Elp3.mono.pdb
#SBATCH --time=1-00:00:00
#SBATCH --error fits/search100000_metric_cam_inside0.6/emd_4151_binned.mrc/Elp3.mono.pdb/log_err.txt
#SBATCH --output fits/search100000_metric_cam_inside0.6/emd_4151_binned.mrc/Elp3.mono.pdb/log_out.txt
#SBATCH --mem=1000

/g/kosinski/kosinski/devel/efitter/scripts/fit_with_chimera --pypath /g/kosinski/kosinski/devel/efitter/ --move_to_center  models/Elp3.mono.pdb fits/search100000_metric_cam_inside0.6/emd_4151_binned.mrc/Elp3.mono.pdb fits/search100000_metric_cam_inside0.6/emd_4151_binned.mrc/config.txt
