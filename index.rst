.. ElongatorTutorial documentation master file, created by
   sphinx-quickstart on Tue Nov 24 22:03:42 2020.

Tutorial: integrative modeling of Elongator complex using Assembline
====================================================================

`Assembline <https://www.embl-hamburg.de/Assembline>`_ is a software for integrative structural modeling of macromolecular assemblies - an assembly line of macromolecular assemblies!

This tutorial demonstrates a basic usage of Assembline on an example of Elongator complex.

Use the **Next** button at the bottom right or menu on the left to navigate through the tutorial.

.. toctree::
   :maxdepth: 2

   about-complex

.. toctree::
   :maxdepth: 2   
   :caption: Set up

   installation

.. toctree::
   :maxdepth: 2   
   :caption: Data preparation

   data_prep

.. toctree::
   :maxdepth: 2   
   :caption: Calculation of fit libraries

   fit_libraries_intro
   efitter_params
   efitter_run
   efitter_analyze

.. toctree::
   :maxdepth: 2   
   :caption: Global optimization

   json_setup
   params_setup
   run_denovo
   analysis_denovo

.. toctree::
   :maxdepth: 2   
   :caption: Refinement

   json_setup_refinement
   params_setup_refinement
   run_refinement
   analysis_refinement

TO DO LIST
----------

.. todolist::
