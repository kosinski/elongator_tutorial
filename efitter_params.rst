Set up
======

#. To run Efitter you need a parameter file in Python language format that specifies:

     * input structures to fit
     * EM map
     * fitting parameters
     * optionally, options for execution on a computer cluster 

#. For this tutorial, the parameter file is already prepared in the ``Elongator`` folder:

    ``efitter_params.py``:
    
    which contains:

    .. code-block:: python

        from efitter import Map
        from string import Template

        method='chimera'
        dry_run = False # Dry run would only print commands it is going to run
        run_crashed_only = False # Only run the jobs that have not delivered output
        master_outdir = 'fits' # relative path to the output, it will be created in the running directory

        MAPS = [
            Map('EM_data/emd_4151_binned.mrc', threshold=0.01, resolution=25),
        ]

        models_dir = 'in_pdbs'

        PDB_FILES = [
            'Elp1_NTD_1st_propeller.pdb',
            'Elp1_NTD_2nd_propeller.pdb',
            'Elp1.CTD.on5cqs.5cqr.model_ElNemo_mode7.pdb',
            'Elp2.pdb',
            'Elp3.mono.pdb',
        ]

        CA_only = False # Calculate the fitting scores using Calpha atoms only?
        backbone_only = False # Calculate the fitting scores using backbone atoms only?
        move_to_center = True # Move the PDB structure to the center of the map?

        # Each element of fitmap_args is a dictionary specifying parameters for a run
        # If multiple dictionaries are specified,
        # the script will run a seperate run for each dictionary for each map-structure combination.
        # E.g. if two maps, three structures, and two paramaters dictionaries are specified,
        # the script will run 2 x 3 x 2 = 12 runs.
        fitmap_args = [
            # Parameters for main runs (https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/fitmap.html)
            {
                'template': Template("""
                    map $map
                    map_threshold $threshold
                    fitmap_args resolution $resolution metric cam maxSteps 100 envelope true search 100000 placement sr clusterAngle 1 clusterShift 1.0 radius 200 inside .60
                    saveFiles False
                    """),
                'config_prefix': 'search100000_metric_cam_inside0.6' # some name informative of the fitmap_args parameters
                }
        ]

        # Adjust the parameters below to run on a cluster or standalone workstation

        #For example, for a cluster with Slurm submission system, use:
        cluster_submission_command = 'sbatch'
        run_script_templ = Template("""#!/bin/bash
        #
        #SBATCH --job-name=$job_name
        #SBATCH --time=1-00:00:00
        #SBATCH --error $pdb_outdir/log_err.txt
        #SBATCH --output $pdb_outdir/log_out.txt
        #SBATCH --mem=1000

        $cmd
        """)
        # The run_script_templ is template for you cluster submission script (using Python string.Template formatting)


        #On a standalone computer
        #1. Remove or comment out the cluster_submission_command and run_script_templ
        #2. Uncomment the following lines
        # run_script_templ = Template("""#!/bin/bash
        # #
        # echo $job_name
        # $cmd &>$pdb_outdir/log&
        # """)


#. Based on the above parameter file, Efitter will run five fitting runs - one for each structure. You can run these as five independent jobs on a computer cluster or standalone workstation.

#. To run on a computer cluster, adjust the ``cluster_submission_command`` and ``run_script_templ`` to the queuing system on your cluster (the provided example would work only on a Slurm cluster). It is important that you include ``$job_name``, ``$pdb_outdir``, and ``$cmd`` in your script template.

#. To run on a standalone workstation, remove or comment out ``cluster_submission_command`` and replace the ``run_script_templ`` with 

    .. code-block:: python
    
        run_script_templ = Template("""#!/bin/bash
        #
        echo $job_name
        $cmd &>$pdb_outdir/log&
        """)

