Setting up the JSON project file
================================

Enter the main directory.


.. note:: You can find a ready-to-use JSON template file named ::

            elongator_refine_template.json

        in the tutorial directory and immediately proceed to the next step of :doc:`params_setup_refinement`.


Generating the file from scratch
--------------------------------

#. Generate the template for the refinement. ::

    gen_refinement_template.py --out_json elongator_refine_template.json --params params.py elongator.json

#. In the following steps, modify the resulting ``elongator_refine_template.json`` template file by adding new restraints and inactivating or removing restraints not needed, adjusting weights.
   
#. Add the traditional cross-correlation-based EM fit restraint to the "data" block in the JSON file:

    .. code-block:: JSON

        {
            "series": [

            ],
            "symmetry": {
                "sym_tr3ds": [

                ]
            },
            "data": [
                {
                    "active": true,
                    "type": "em_map",
                    "name": "FitRestraint",
                    "em_restraint_type": "FitRestraint",
                    "filename": "EM_data/emd_4151_binned.mrc",
                    "threshold": 0.01,
                    "voxel_size": 6.6,
                    "resolution": 25,
                    "weight": 1000,
                    "first_copy_only": true,
                    "repr_resolution": 10,
                    "optimized_components": [
                        {"name": "Elp1", "subunit": "Elp1"},
                        {"name": "Elp2", "subunit": "Elp2"},
                        {"name": "Elp3", "subunit": "Elp3"}

                    ]
                }
            ],
            "subunits": [

            ],
            "xlinkanalyzerVersion": "0.1"
        }

#. Redefine rigid bodies. In this case, we noticed that some crosslinks are in long loops and tails. We will then let them move flexibly in the refinement.
   
    
    To to do this, we add ``rigid_bodies`` block to the JSON file, in which we define rigid bodies with ``resi_ranges``, excluding the selected loops:

    .. code-block:: JSON

        {
            "series": [

            ],
            "rigid_bodies": [
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp1_1", "subunit": "Elp1", "domain": "propeller1"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp1_2", "subunit": "Elp1", "domain": "propeller2"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "components": [
                        { "name": "Elp1_3", "subunit": "Elp1", "domain": "CTD", "copies": [0, 1]}
                    ],
                    "freeze": true
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp2", "subunit": "Elp2", 
                            "resi_ranges": [
                                [1, 239],
                                [257, 788]
                            ]
                        }
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp3", "subunit": "Elp3", 
                            "resi_ranges": [
                                [95, 468],
                                [477, 557]
                            ]}
                    ]
                }
            ],
            "symmetry": {
                "sym_tr3ds": [

                ]
            },
            "data": [

            ],
            "subunits": [

            ],
            "xlinkanalyzerVersion": "0.1"
        }

    .. note:: Whenever you define custom rigid bodies here, set ``add_rbs_from_pdbs = False`` in the parameter file.    
    
#. Add symmetry restraint for the C-terminal domain of Elp1 (CTD).
   
    Why? The input structure for the CTD domain is already a symmetrical dimer and the entire dimer is defined as a rigid body. This domain cannot be constrained by the symmetry "constraint" because the constraints do not work "within rigid bodies".

    To define the symmetry restraint, add the following block:

    .. code-block:: JSON

        {
            "series": [

            ],
            "rigid_bodies": [

            ],
            "symmetry": {
                "sym_tr3ds": [

                ]
                "apply_symmetry": [
                    {
                        "sym": "2fold",
                        "restraint_type": "symmetry_restraint",
                        "selectors": [
                            {"subunit": "Elp1", "serie": "2fold", "copies": [0], "domain": "CTD"},
                            {"subunit": "Elp1", "serie": "2fold", "copies": [1], "domain": "CTD"}
                        ]
                    }
                ]
            },
            "data": [

            ],
            "subunits": [

            ],
            "xlinkanalyzerVersion": "0.1"
        }

    .. note:: Whenever you define symmetry restraints here, in the parameter file set ``add_symmetry_restraints = True`` and add ``'sym_restraints'`` to the scoring function.    
   

