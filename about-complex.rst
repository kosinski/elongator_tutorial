About the Elongator complex
===========================

Elongator is a complex involved in tRNA modification.
In yeast, it contains six subunits, two copies each.

We built and published an integrative model of yeast `Elongator in 2017 <https://pubmed.ncbi.nlm.nih.gov/27974378/>`_. The cryo-EM structure `published in 2019 <https://advances.sciencemag.org/content/5/7/eaaw2326>`_ confirmed the model.

.. image:: images/Elongator_model_vs_cryoem.jpg
  :width: 800
  :alt: Elongator model vs cryoEM



In this tutorial, we will model a subcomplex of Elongator composed of subunits named Elp1, Elp2, and Elp3, using the real data from the 2017 work.

The following PDB files are available for the subunits:

.. image:: images/Elongator_system.jpg
  :width: 800
  :alt: Elongator system

The following experimental data will be used as restraints:

* a negative stain EM map at 27 Å (`EMD-4151 <https://www.ebi.ac.uk/pdbe/entry/emdb/EMD-4151>`_):

    The map exhibits a C2 symmetry, thus the model will be built with C2 symmetry.

* crosslinks from a crosslinking mass spectrometry experiment (`details here <https://pubmed.ncbi.nlm.nih.gov/27974378/>`_)
