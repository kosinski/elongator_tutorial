Data preparation
================

Download the data for this tutorial from `here <https://git.embl.de/kosinski/elongator_tutorial/-/archive/master/elongator_tutorial-master.zip?path=Elongator>`_ 

Create a directory for your project and gather your data there. For this tutorial the data files are organized as follows: ::

    Elongator/

        # Electron microscopy map
        EM_data/
            emd_4151_binned.mrc

        #input structures for modeling
        in_pdbs/
            Elp1.CTD.on5cqs.5cqr.model_ElNemo_mode7.pdb
            Elp1_NTD_1st_propeller.pdb
            Elp1_NTD_2nd_propeller.pdb
            Elp2.pdb
            Elp3.flex_helix.pdb
            Elp3.helix4.pdb
            Elp3.mono.pdb

        #FASTA-formatted file with sequences of all subunits
        elp_sequences.fasta

        #Crosslink files in xQuest or Xlink Analyzer format
        xlinks/
            DSS/
                inter_run3_190412.clean_strict.csv
                intra_run3_190412.clean_strict.csv
                loop_run3_190412.clean_strict.csv
                mono_run3_190412.clean_strict.csv
                sg1-inter.clean_strict.csv
                sg1-intra.clean_strict.csv
                sg1-loop.clean_strict.csv
                sg2-3-inter.clean_strict.csv
                sg2-3-intra.clean_strict.csv
                sg2-3-loop.clean_strict.csv
                sg2-3-mono.clean_strict.csv
            DSG/
                inter_dsg.clean_strict.csv
                inter_dsg_repeat.clean_strict.csv
                intra_dsg.clean_strict.csv
                intra_dsg_repeat.clean_strict.csv
                loop_dsg.clean_strict.csv
                loop_dsg_repeat.clean_strict.csv
                mono_dsg.clean_strict.csv
                mono_dsg_repeat.clean_strict.csv
