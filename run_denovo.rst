Run
===


#. If you haven't yet, activate the environment before using the software by

    .. code-block:: bash

        source activate Assembline

    or depending on your computer setup:

    .. code-block:: bash

        conda activate Assembline


#. Enter the main project directory

#. Run a single run for testing

    .. code-block:: bash

        assembline.py  --traj --models --prefix 0000000 -o out elongator.json params.py

    Output is saved to ``out`` directory specified by ``-o`` option.

#. Run 1000 runs to build 1000 models

    * Method 1: Submit all runs to the computer cluster or run on a workstation in chunks of N according to the number of processors:

        .. code-block:: bash

            assembline.py --traj --models -o out --multi --start_idx 0 --njobs 1000 elongator.json params.py

        * on a cluster, this will submit 1000 modeling jobs in the queue, each job leading to one model (if ntasks in params.py is set to 1)
        * if ntasks params.py is N, it will run submit 1000/N cluster jobs, each running N modeling jobs
        * on a multicore computer, it will run ntasks at a time, and keep running until all 1000 jobs are done. 

        .. note:: The number of processors or cluster submission commands and templates are specified in ``params.py``


    * Method 2: Dynamically adjust the number of concurrent runs (e.g. to not to overload a cluster or annoy other users):
      
        .. warning:: The following works out of the box only on the EMBL cluster. To adjust to your cluster, modify the ``assembline.py`` for your cluster environment following the guidelines in the script. 

        .. code-block:: bash

            assembline.py \
            --traj \
            --models \
            --multi \
            --daemon \
            --min_concurrent_jobs 200 \
            --max_concurrent_jobs 1000 \
            -o out \
            --start_idx 0 \
            --njobs 1000 \
            elongator.json params.py &>log&

    * Method 3: If none of the above solutions work for you, you could probably submit multiple jobs manually using a mad shell loop e.g. on a computer cluster with the Slurm queuing system:
      
        .. code-block:: bash
      
            for i in $(seq -f "%07g" 0 999)
                do
                    srun assembline.py  --traj --models --prefix $i -o out elongator.json params.py &>log&
                done

        Just remember to make the ``prefix`` unique for every run.

    Read more about how to run many runs on different platforms in the `manual <https://assembline.readthedocs.io/en/latest/run.html>`_