Setting up the parameter file
=============================

You can find a ready-to-use parameter file named ::

    params.py

in the tutorial directory.

The file is in the Python format and contains the following content:

    .. code-block:: python

        protocol = 'denovo_MC-SA'

        SA_schedule = [
            (50000,   10000),
            (10000,   10000),
            (5000,   10000),
            (1000,   50000),
            (100,   50000)
        ]

        do_ini_opt = True
        ini_opt_SA_schedule = [
            (1000, 1000)
        ]

        traj_frame_period = 10
        print_frame_period = traj_frame_period

        print_log_scores_to_files = True
        print_log_scores_to_files_frame_period = 10

        print_total_score_to_files = True
        print_total_score_to_files_frame_period = 10


        struct_resolutions = [0,10] 
        add_rbs_from_pdbs = True
        connectivity_restraint_weight = 1.
        conn_first_copy_only = True

        add_symmetry_constraints = True


        add_xlink_restraints = True
        x_xlink_score_type = 'LogHarmonic'
        x_min_ld_score = 25
        x_weight = 1000.0
        x_xlink_distance = 15

        discrete_restraints_weight=10000 

        ev_restraints = [
            {
                'name': 'ev_restraints_lowres',
                'weight': 10,
                'repr_resolution': 10
            }
        ]

        scoring_functions = {
            'score_func_ini_opt': {
                'restraints': [
                    'discrete_restraints',
                    'conn_restraints',
                    'ev_restraints_lowres'
                ]
            },
            'score_func_lowres': {
                'restraints': [
                    'discrete_restraints',
                    'xlink_restraints',
                    'conn_restraints',
                    'ev_restraints_lowres'
                ]
            }
        }

        score_func = 'score_func_lowres'
        score_func_ini_opt = 'score_func_ini_opt'

        ntasks = 1
        cluster_submission_command = 'sbatch'
        from string import Template
        run_script_templ = Template("""#!/bin/bash
        #
        #SBATCH --ntasks=1
        #SBATCH --cpus-per-task=$ntasks
        #SBATCH --mem-per-cpu=2000
        #SBATCH --job-name=${prefix}fit
        #SBATCH --time=00:30:00
        #SBATCH -e $outdir/logs/${prefix}_err.txt
        #SBATCH -o $outdir/logs/${prefix}_out.txt

        echo "Running on:"
        srun hostname

        $cmd

        wait
        """)









To generate this file yourself from scratch:

#. Download the template `params_combinations.py file <https://git.embl.de/kosinski/Assembline/-/blob/master/doc/templates/params_combinations.py>`_

#. Open the file in an editor to adjust the parameters as in the provided example.
   
   See `parameters <https://assembline.readthedocs.io/en/latest/params.html>`_ for explanations and description of available parameters.




