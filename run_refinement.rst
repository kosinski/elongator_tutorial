Run
===

This shows how to refine top all models, running three refinement runs for each model.

#. I you haven't yet, activate the environment before using the software by

    .. code-block:: bash

        source activate Assembline

    or depending on your computer setup:

    .. code-block:: bash
    
        conda activate Assembline

#. Enter the main project directory

#.  Setup a refinement directory to refine all 1000 models from the global optimization

    .. code-block:: bash

        setup_refine.py \
            --scores out/all_scores.csv \
            --previous_json elongator.json \
            --refine_json_template elongator_refine_template.json \
            --refine_json_outname elongator_refine.json \
            --previous_outdir out/\
            --refine_outdir out/refinement

    This will create a directory ``out/refinement``, which will contain 1000 sub-directories.
    Each of the subdirectories contains input files for refining each of the 1000 scoring models.
    The input files include PDB files oriented as in the models and JSON files for referring to those PDB files.

#. Run a test ::

    model_id=`ls --color=never out/refinement | head -n 1`
    assembline.py --traj --models --prefix 0000000 -o out/refinement/"$model_id"/out out/refinement/"$model_id"/elongator_refine.json params_refine.py
    rm -r out/refinement/"$model_id"/out

#. Refine the 1000 models, running 3 refinement runs for each model (3000 runs in total). 

    For example like this:
    
    .. code-block:: bash

        for model_id in `ls --color=never out/refinement`;
            do
                echo $model_id
                assembline.py --models -o out/refinement/"$model_id"/out --multi --start_idx 0 --njobs 3 --prefix refine_"$model_id" out/refinement/"$model_id"/elongator_refine.json params_refine.py
            done

    This will navigate to all subdirectories of the ``out/refinement`` and run the refinements there.
    Note no ``--traj`` option to save disk space.
