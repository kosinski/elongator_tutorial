Run
===

#. Rename the `fits` directory provided in the tutorial to `fits_bk` (the fitting in the next step will write to `fits` directory)

#. Run the fitting
   
    .. code-block:: bash

        fit.py efitter_params.py 

    The fitting runs will execute in the background and take two to three hours.

    The script will create the directory called ``fits`` with the following content::

        fits/
            search100000_metric_cam_inside0.6/ #directory with output for the given set of parameters
                emd_4151_binned.mrc/ #directory with all fits for this map for this parameters

                    #directories with names as the PDB files
                    Elp1.CTD.on5cqs.5cqr.model_ElNemo_mode7.pdb/
                    Elp1_NTD_1st_propeller.pdb/
                    Elp1_NTD_2nd_propeller.pdb/
                    Elp2.pdb/
                    Elp3.mono.pdb/
                    config.txt
                    emd_4151_binned.mrc

    Each of the ``.pdb`` directories should contain the following files::

        emd_4151_binned.mrc #link to the map file
        log_err.txt #error messages
        log_out.txt #output messages
        ori_pdb.pdb #link to the original PDB file
        solutions.csv #file with transformation matrices defining the fits

    
    .. note:: The fitting is complete when each of the ``.pdb`` directories contains ``solutions.csv`` file.

        Inspect the ``log_out.txt`` files for status and ``log_err.txt`` for error messages.

#. Upon completion, calculate p-values:
   

    .. code-block:: bash

        genpval.py fits

    This should create additional files in each ``.pdb`` directory::

        Rplots.pdf
        solutions_pvalues.csv

    The ``solutions_pvalues.csv`` is crucial for the global optimization step.